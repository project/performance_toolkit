<?php

/**
 * @file
 * Contains PerformanceToolkitSelectQueries class.
 */

$plugin = array(
  'name' => 'DB select vs DB query performance',
  'handler' => array(
    'class' => 'PerformanceToolkitSelectQueries',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path'  => 'select_queries',
    'name' => t('db_select vs db_query', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitSelectQueries implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 1000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t("The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations), $context);
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("Select with db_query function is faster but do not forget about advantages of db_select for dynamic queries.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'dbQuery',
      'dbSelect',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbQuery($n, & $variant) {
    $variant['title'] = 'DB query';

    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_query('SELECT nid FROM {performance_toolkit_node} WHERE nid = :d', array(':d' => $i))->fetchField();
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbSelect($n, & $variant) {
    $variant['title'] = 'DB select query';

    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_select('performance_toolkit_node', 'n')
        ->fields('n', array('nid'))
        ->condition('nid', $i)
        ->execute()
        ->fetchField();
    }
  }

}
