<?php

/**
 * @file
 * Contains PerformanceToolkitStringsTrim class.
 */

$plugin = array(
  'name' => 'Trim function vs regular expressions',
  'handler' => array(
    'class' => 'PerformanceToolkitStringsTrim',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path' => 'php_string_trim',
    'name' => t('String trim', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

class PerformanceToolkitStringsTrim implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("You have to check performance of any regular expression you have.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'trimUse',
      'regularExpressionsUse',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function trimUse($n, & $variant) {
    $variant['title'] = 'Native trim() function';

    $string = 'Minsk is the capital of Belarus,';
    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = trim($string, ",");
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function regularExpressionsUse($n, & $variant) {
    $variant['title'] = 'Trim with regular expression';

    $string = 'Minsk is the capital of Belarus,';
    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = preg_replace('/^,*|,*$/m', "", $string);
    }
  }

}
