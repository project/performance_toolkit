<?php

/**
 * @file
 * Contains PerformanceToolkitStringsConcatenation class.
 */

$plugin = array(
  'name' => 'Concatenation for strings in quotes',
  'handler' => array(
    'class' => 'PerformanceToolkitStringsConcatenation',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path'  => 'php_string_concatenation',
    'name' => t('Strings concatenation', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

class PerformanceToolkitStringsConcatenation implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("It's a myth, we can use variables inside double quotes and it will be as fast as regular concatenation with single quote string.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'oneQuote',
      'twoQuotes',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function oneQuote($n, & $variant) {
    $variant['title'] = 'Double quotes concatenation';

    $country = 'Belarus';
    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = "Minsk is the capital of $country";
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function twoQuotes($n, & $variant) {
    $variant['title'] = 'Single quotes concatenation';

    $country = 'Belarus';
    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = 'Minsk is the capital of ' . $country;
    }
  }

}
