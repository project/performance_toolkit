<?php

/**
 * @file
 * Contains PerformanceToolkitRenderTag class.
 */

$plugin = array(
  'name' => 'Render with <em>tag</em> vs <em>markup</em>',
  'handler' => array(
    'class' => 'PerformanceToolkitRenderTag',
  ),
  'category' => performance_toolkit_get_available_categories(0),
  'plugin' => array(
    'path' => 'theme',
    'name' => t('Render with tag element', array(), performance_toolkit_get_context()),
  ),
  'weight' => 1,
);

class PerformanceToolkitRenderTag implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("Do not use tag for any simple HTML markup. Use #markup element instead.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'renderWithTag',
      'renderWithMarkup',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function renderWithTag($n, & $variant) {
    $variant['title'] = 'Extra tag using';

    $page['highlighted']['new_stuff'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => 'my-container'),
    );

    $text = t('Heading');
    for ($i = 0; $i < $n; $i++) {
      $page['highlighted']['new_stuff'][$i] = array(
        '#type'  => 'html_tag',
        '#tag'   => 'h2',
        '#value' => $text,
        '#attributes' => array('class' => 'my-heading'),
      );
    }

    $variant['results'][] = drupal_render($page);
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function renderWithMarkup($n, & $variant) {
    $variant['title'] = 'Markup using';

    $page['highlighted']['new_stuff'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => 'my-container'),
    );

    $text = t('Heading');
    for ($i = 0; $i < $n; $i++) {
      $page['highlighted']['new_stuff'][$i] = array(
        '#markup' => '<h2 class="my-heading">' . $text . '</h2>',
      );
    }

    $variant['results'][] = drupal_render($page);
  }

}
