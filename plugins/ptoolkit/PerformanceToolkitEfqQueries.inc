<?php

/**
 * @file
 * Contains PerformanceToolkitEfqQueries class.
 */

$plugin = array(
  'name' => 'DB select vs EFQ performance for simple queries',
  'handler' => array(
    'class' => 'PerformanceToolkitEfqQueries',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'efq_queries',
    'name' => t('EFQ simple vs DB select', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitEfqQueries implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 1000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $users = db_select('users', 'u')
      ->fields('n', array('uid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($users < $iterations) {
      $requirements['message'] = t(
        "The amount of users - %users is less then iterations %i, so results may not be demonstrative",
        array('%users' => $users, '%i' => $iterations),
        $context
      );
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t(
      "You have to use use EFQ queries only in case you need filtration for entity field values",
      array(),
      $context
    );
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'dbEfq',
      'dbSelect',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbEfq($n, & $variant) {
    $query = new EntityFieldQuery();
    $users = $query->entityCondition('entity_type', 'user')
      ->range(0, $n)
      ->execute();
    $users = array_keys($users['user']);

    $variant['results'] = $users;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbSelect($n, & $variant) {
    $variant['title'] = 'Fine all user rows by DB select query';
    $variant['results'] = db_select('users', 'u')
      ->fields('u', array('uid'))
      ->range(0, $n)
      ->execute()
      ->fetchAllAssoc('uid');
  }

}
