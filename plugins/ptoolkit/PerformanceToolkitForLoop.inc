<?php

/**
 * @file
 * Contains PerformanceToolkitForLoop class.
 */

$plugin = array(
  'name' => 'Variable increment',
  'handler' => array(
    'class' => 'PerformanceToolkitForLoop',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path' => 'php_for_loop',
    'name' => t('Incorrect "for" usage', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

/**
 * Class PerformanceToolkitForLoop.
 */
class PerformanceToolkitForLoop implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 100000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("Do not use count() inside 'for' loop - it will be fired for any element of the array", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'incorrectLoop',
      'correctLoop',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function incorrectLoop($n, & $variant) {
    $variant['title'] = 'Incorrect loop';

    $test_array = range(0, $n);
    for ($k = 0; $k < count($test_array); $k++) {
      $variant['results'] = $test_array[$k];
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function correctLoop($n, & $variant) {
    $variant['title'] = 'Correct loop';

    $test_array = range(0, $n);
    $count = count($test_array);
    for ($k = 0; $k < $count; $k++) {
      $variant['results'] = $test_array[$k];
    }
  }

}
