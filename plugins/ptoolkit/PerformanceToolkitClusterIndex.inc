<?php

/**
 * @file
 * Contains PerformanceToolkitClusterIndex class.
 */

$plugin = array(
  'name' => 'Cluster index usage',
  'handler' => array(
    'class' => 'PerformanceToolkitClusterIndex',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'query_cluster_index',
    'name' => t('Cluster Index', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitClusterIndex implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 5000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t(
        "The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations),
        $context
      );
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t(
      "INNODB using cluster index for primary key.",
      array(),
      $context
    );
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'clusterIndex',
      'secondaryIndex',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function clusterIndex($n, & $variant) {
    $variant['title'] = 'With cluster index';

    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_select('performance_toolkit_node', 'n')
        ->fields('n')
        ->condition('nid', $i)
        ->execute()
        ->fetchAssoc();
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function secondaryIndex($n, & $variant) {
    $variant['title'] = 'Without cluster index';

    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_select('performance_toolkit_node', 'n')
        ->fields('n')
        ->condition('vid', $i)
        ->execute()
        ->fetchAssoc();
    }
  }

}
