<?php

/**
 * @file
 * Contains PerformanceToolkitStringsInQuotes class.
 */

$plugin = array(
  'name' => 'Strings in quotes',
  'handler' => array(
    'class' => 'PerformanceToolkitStringsInQuotes',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path' => 'php_quotes',
    'name' => t('String Quotes', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

class PerformanceToolkitStringsInQuotes implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("It's a myth that native sprintf() function is faster than variables replacement in double quotes strings.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'oneQuote',
      'sprintfUsage',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function oneQuote($n, & $variant) {
    $variant['title'] = 'Double quotes replacement';

    $city = 'Minsk';
    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = "There are $k monkeys in the $city. The combination of both {$k}{$city}";
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function sprintfUsage($n, & $variant) {
    $variant['title'] = 'A sprintf() replacement';

    $city   = 'Minsk';
    $format = 'There are %1$d monkeys in the %2$s. The combination of both %1$d%2$s';

    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = sprintf($format, $k, $city);
    }
  }

}
