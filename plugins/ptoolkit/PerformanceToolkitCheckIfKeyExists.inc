<?php

/**
 * @file
 * Contains PerformanceToolkitCheckIfKeyExists class.
 */

$plugin = array(
  'name' => 'Checking for the existing of a key in array',
  'handler' => array(
    'class' => 'PerformanceToolkitCheckIfKeyExists',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path' => 'php_array_check',
    'name' => t('Array key check', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

class PerformanceToolkitCheckIfKeyExists implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 50000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t(
      "Isset is not a function rather language construction and it's very fast in comparison with regular expressions.",
      array(),
      $context
    );
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'issetUse',
      'arrayKeyExistsUse',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function issetUse($n, & $variant) {
    $variant['title'] = 'Check with Isset';

    for ($i = 0; $i < $n; ++$i) {
      $arr[] = rand();
    }

    for ($k = 0; $k < $n; $k++) {
      if (isset($arr[$k])) {
        $variant['results'][] = $arr[$k];
      }
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function arrayKeyExistsUse($n, & $variant) {
    $variant['title'] = 'Check with array_key_exists function';

    $arr = array();
    for ($i = 0; $i < $n; ++$i) {
      $arr[] = rand();
    }

    for ($k = 0; $k < $n; $k++) {
      if (array_key_exists($k, $arr)) {
        $variant['results'][] = $arr[$k];
      }
    }
  }

}
