<?php

/**
 * @file
 * Contains PerformanceToolkitIncrement class.
 */

$plugin = array(
  'name' => 'Variable increment',
  'handler' => array(
    'class' => 'PerformanceToolkitIncrement',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path' => 'php_increment',
    'name' => t('Increment', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

/**
 * Class PerformanceToolkitIncrement.
 */
class PerformanceToolkitIncrement implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;
  public $increment = 1;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("Object property increment should be a little slower in comparison with increment for local variable.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'propertyIncrement',
      'localIncrement',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function propertyIncrement($n, & $variant) {
    $variant['title'] = 'Increment of object property';

    for ($k = 0; $k < $n; $k++) {
      $variant['results'] = $this->increment++;
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function localIncrement($n, & $variant) {
    $variant['title'] = 'Increment of local variable';

    $local = 1;
    for ($k = 0; $k < $n; $k++) {
      $variant['results'] = $local++;
    }
  }

}
