<?php

/**
 * @file
 * Contains PerformanceToolkitSelectQueriesInterval class.
 */

$plugin = array(
  'name' => "Comparison of interval queries",
  'handler' => array(
    'class' => 'PerformanceToolkitSelectQueriesInterval',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'query_interval',
    'name' => t('Query interval', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitSelectQueriesInterval implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 1000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t("The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations), $context);
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("BEETWEEN works faster then IN", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'dbQueryIn',
      'dbSelectIn',
      'dbSelectBetween',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbSelectBetween($n, & $variant) {
    $variant['title'] = 'BEETWEEN';

    $variant['results'] = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('nid', array(0, $n), 'BETWEEN')
      ->execute()
      ->fetchAll();
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbSelectIn($n, & $variant) {
    $variant['title'] = 'IN db_select';

    $nids = range(0, $n);
    $variant['results'] = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('nid', $nids, 'IN')
      ->execute()
      ->fetchAll();
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbQueryIn($n, & $variant) {
    $variant['title'] = 'IN db_query';

    $nids = range(0, $n);
    $query = db_query("SELECT nid FROM performance_toolkit_node WHERE nid IN (':nids')", array(':nids' => $nids));
    $variant['results'] = $query->fetchAll();
  }

}
