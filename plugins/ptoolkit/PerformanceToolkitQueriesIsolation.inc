<?php

/**
 * @file
 * Contains PerformanceToolkitQueriesIsolation class.
 */

$plugin = array(
  'name' => 'Column isolation principle',
  'handler' => array(
    'class' => 'PerformanceToolkitQueriesIsolation',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'join_column_isolation',
    'name' => t('Column Isolation', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitQueriesIsolation implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 1000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $rows = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($rows < $iterations) {
      $requirements['message'] = t(
        "The amount of records - %rows is less then amount of iterations - %i, so results may not be demonstrative",
        array('%rows' => $rows, '%i' => $iterations),
        $context
      );
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("We can't use any expression for the left part in where clause.
    In this case INNODB storage can't use index for the query.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'phpEvaluation',
      'mysqlEvaluation',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function phpEvaluation($n, & $variant) {
    $variant['title'] = 'Left side condition';

    db_query('RESET QUERY CACHE');
    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_query(
        'SELECT nid FROM {performance_toolkit_node} WHERE nid + 1 = :d',
        array(':d' => $i)
      )->fetchField();
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function mysqlEvaluation($n, & $variant) {
    $variant['title'] = 'Right side condition';

    db_query('RESET QUERY CACHE');
    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_query(
        'SELECT nid FROM {performance_toolkit_node} WHERE nid = :d - 1',
        array(':d' => $i)
      )->fetchField();
    }
  }

}
