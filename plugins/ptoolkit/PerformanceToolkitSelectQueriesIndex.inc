<?php

/**
 * @file
 * Contains PerformanceToolkitSelectQueriesIndex class.
 */

$plugin = array(
  'name' => 'If we do not have the column in index we can evaluate condition on PHP side',
  'handler' => array(
    'class' => 'PerformanceToolkitSelectQueriesIndex',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path'  => 'query_index_condition',
    'name' => t('Query index', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitSelectQueriesIndex implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("Condition can be done by PHP in case of we have query with pure selectivity for conditions.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'dbQuery',
      'dbSelect',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbQuery($n, & $variant) {
    $variant['title'] = 'Mysql side evaluation';

    $query = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid', 'vid', 'title', 'promote'))
      ->condition('promote', 1)
      ->range(0, $n)
      ->execute();

    foreach ($query as $node) {
      $variant['results'][] = $node;
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbSelect($n, & $variant) {
    $variant['title'] = 'PHP side evaluation';

    $query = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid', 'vid', 'title', 'promote'))
      ->range(0, $n)
      ->execute();

    foreach ($query as $node) {
      if ($node->promote) {
        $variant['results'][] = $node;
      }
    }
  }

}
