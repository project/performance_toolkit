<?php

/**
 * @file
 * Contains PerformanceToolkitSelectLikeQueries class.
 */

$plugin = array(
  'name' => 'DB select vs DB query performance',
  'handler' => array(
    'class' => 'PerformanceToolkitSelectLikeQueries',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'query_like',
    'name' => t('Query with "Like"', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitSelectLikeQueries implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 1000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t("The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations), $context);
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("MYSQL can't use index for query with 'Like condition' requested for prefix - '%A',
    but can use index for suffix - 'A%'.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'likeQuerySuffix',
      'likeQueryPrefix',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function likeQuerySuffix($n, & $variant) {
    $variant['title'] = 'Suffix condition';

    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_select('performance_toolkit_node', 'n')
        ->fields('n', array('title'))
        ->condition('title', db_like('A') . '%', 'LIKE')
        ->execute()
        ->fetchAllAssoc('title');
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function likeQueryPrefix($n, & $variant) {
    $variant['title'] = 'Prefix condition';

    for ($i = 1; $i < $n; $i++) {
      $variant['results'] = db_select('performance_toolkit_node', 'n')
        ->fields('n', array('title'))
        ->condition('title', '%' . db_like('A'), 'LIKE')
        ->execute()
        ->fetchAllAssoc('title');
    }
  }

}
