<?php

/**
 * @file
 * Contains PerformanceToolkitInsertsQueries class.
 */

$plugin = array(
  'name' => 'Database insert queries',
  'handler' => array(
    'class' => 'PerformanceToolkitInsertsQueries',
  ),
  'category' => performance_toolkit_get_available_categories(0),
  'plugin' => array(
    'path'  => 'insert_queries',
    'name' => t('Insert queries', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitInsertsQueries implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 1000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();

    if (!module_exists('dblog')) {
      $requirements['message'] = t("DBlog module is disabled. Please enable it for demonstrative purpose", array(), $context);
      $requirements['access'] = FALSE;
    }
    else {
      $requirements['message'] = t("Please notice we have added few records to watchdog table", array(), $context);
    }

    return $requirements;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion= t("Use db_insert with 'values' method rather a lot of db_insert for big amount of records.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'dbInsertValues',
      'dbInsert',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbInsert($n, & $variant) {
    $variant['title'] = 'DB insert in loop';

    $node_type = 'article';

    $result = db_query_range("SELECT uid FROM {" . PERFORMANCE_TOOLKIT_USERS_TABLE . "}", 0, 50);
    foreach ($result as $record) {
      $users[$record->uid] = $record->uid;
    }

    for ($i = 0; $i < $n; $i++) {
      $node['uid']     = array_rand(array_values($users));
      $node['type']    = $node_type;
      $node['title']   = 'Test title for performance compare module';
      $node['promote'] = mt_rand(0, 1);
      $node['created'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);
      $node['changed'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);
      $node['sticky']  = mt_rand(0, 1);

      if (function_exists('devel_create_greeking')) {
        $node['title'] = devel_create_greeking(1, TRUE);
      }

      $query = db_insert(PERFORMANCE_TOOLKIT_NODE_TABLE)->fields(array_keys($node));
      $query->execute();
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbInsertValues($n, & $variant) {
    $variant['title'] = 'DB inserts with values method';

    $node_types = array(
      'article' => 'article',
      'page'    => 'page',
    );

    $result = db_query_range("SELECT uid FROM {" . PERFORMANCE_TOOLKIT_USERS_TABLE . "}", 0, 50);
    foreach ($result as $record) {
      $users[$record->uid] = $record->uid;
    }

    $node = array(
      'uid'     => '',
      'type'    => '',
      'title'   => '',
      'promote' => '',
      'created' => '',
      'changed' => '',
      'sticky'  => '',
    );

    $query = db_insert(PERFORMANCE_TOOLKIT_NODE_TABLE)->fields(array_keys($node));
    for ($i = 0; $i < $n; $i++) {
      $node['uid']     = array_rand(array_values($users));
      $node['uid']     = array_rand(array_values($users));
      $node['type']    = array_rand($node_types);
      $node['promote'] = mt_rand(0, 1);
      $node['sticky']  = mt_rand(0, 1);
      $node['created'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);
      $node['changed'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);

      if (function_exists('devel_create_greeking')) {
        $node['title'] = devel_create_greeking(1, TRUE);
      }
      else {
        $node['title'] = 'Test title for performance compare module';
      }

      $query->values(array_values($node));
    }

    $query->execute();
  }
}
