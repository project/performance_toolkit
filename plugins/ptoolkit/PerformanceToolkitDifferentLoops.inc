<?php

/**
 * @file
 * Contains PerformanceToolkitDifferentLoops class.
 */

$plugin = array(
  'name' => 'Variable increment',
  'handler' => array(
    'class' => 'PerformanceToolkitDifferentLoops',
  ),
  'category' => performance_toolkit_get_available_categories(2),
  'plugin' => array(
    'path' => 'php_loops',
    'name' => t('PHP loops', array(), performance_toolkit_get_context()),
  ),
  'weight' => 3,
);

/**
 * Class PerformanceToolkitDifferentLoops.
 */
class PerformanceToolkitDifferentLoops implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 100000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t(
      "Foreach, For, and While loops have relatively the same speed",
      array(),
      $context
    );
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'whileLoop',
      'forLoop',
      'foreachLoop',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function whileLoop($n, & $variant) {
    $variant['title'] = 'While loop';

    $i = 0;
    while ($i < $n) {
      $variant['results'][] = '12345678910';
      $i++;
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function forLoop($n, & $variant) {
    $variant['title'] = 'For loop';

    for ($k = 0; $k < $n; $k++) {
      $variant['results'][] = '12345678910';
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function foreachLoop($n, & $variant) {
    $variant['title'] = 'Foreach loop';

    $items = array_fill(0, $n, '12345678910');
    foreach ($items as $key => $value) {
      $variant['results'][] = $items[$key];
    }
  }

}
