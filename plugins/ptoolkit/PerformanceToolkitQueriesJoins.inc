<?php

/**
 * @file
 * Contains PerformanceToolkitQueriesJoins class.
 */

$plugin = array(
  'name' => 'Base Tables and Join Order',
  'handler' => array(
    'class' => 'PerformanceToolkitQueriesJoins',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'joins',
    'name' => t('Joins order', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitQueriesJoins implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 25000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t("The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations), $context);
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("The base table of Join should return a smaller set of results.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'joinNodeBase',
      'joinUserBase',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function joinUserBase($n, & $variant) {
    $variant['title'] = 'Users as a base table';

    $variant['results'] = db_query_range('
      SELECT STRAIGHT_JOIN u.uid, u.name, n.nid, n.title
      FROM {performance_toolkit_users} u 
      INNER JOIN {performance_toolkit_node} n 
      ON n.uid = u.uid
      WHERE u.uid = :uid',
      0, $n, array(':uid' => 10))
      ->fetchAllAssoc('nid');
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function joinNodeBase($n, & $variant) {
    $variant['title'] = 'Node as a base table';

    $variant['results'] = db_query_range('
      SELECT STRAIGHT_JOIN u.uid, u.name, n.nid, n.title 
      FROM {performance_toolkit_node} n 
      INNER JOIN {performance_toolkit_users} u 
      ON u.uid = n.uid
      WHERE u.uid = :uid',
      0, $n, array(':uid' => 10))
      ->fetchAllAssoc('nid');
  }

}
