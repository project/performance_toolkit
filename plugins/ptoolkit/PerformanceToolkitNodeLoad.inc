<?php

/**
 * @file
 * Contains PerformanceToolkitNodeLoad class.
 */

$plugin = array(
  'name' => 'Load node entities',
  'handler' => array(
    'class' => 'PerformanceToolkitNodeLoad',
  ),
  'category' => performance_toolkit_get_available_categories(0),
  'plugin' => array(
    'path' => 'entity_load',
    'name' => t('Entity load', array(), performance_toolkit_get_context()),
  ),
  'weight' => -100,
);

class PerformanceToolkitNodeLoad implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 200;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t(
        "The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations),
        $context
      );
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t(
      "If we need to load multiple entities we have to use entity_load_multiple().
    It's extremely fast in comparison with entity_load().",
      array(),
      $context
    );
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'nodeLoad',
      'entityLoadMultiple',
      'nodeLoadMultiple',
      'entityLoad',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function nodeLoad($n, & $variant) {
    $variant['title'] = 'Node load';

    $results = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->range(0, $n)
      ->execute()
      ->fetchAllKeyed(0, 0);

    foreach ($results as $nid) {
      $variant['results'][] = node_load($nid, NULL, TRUE);
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function entityLoad($n, & $variant) {
    $variant['title'] = 'Entity load';

    $results = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->range(0, $n)
      ->execute()
      ->fetchAllKeyed(0, 0);

    foreach ($results as $nid) {
      $variant['results'][] = entity_load('node', array($nid), array(), TRUE);
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function entityLoadMultiple($n, & $variant) {
    $variant['title'] = 'Entity load multiple';

    $results = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->range(0, $n)
      ->execute()
      ->fetchAllKeyed(0, 0);

    $variant['results'] = entity_load('node', $results, array(), TRUE);
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function nodeLoadMultiple($n, & $variant) {
    $variant['title'] = 'Node load multiple';

    $results = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->range(0, $n)
      ->execute()
      ->fetchAllKeyed(0, 0);

    $variant['results'] = node_load_multiple($results, array(), TRUE);
  }

}
