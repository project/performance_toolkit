<?php

/**
 * @file
 * Contains PerformanceToolkitSelectQueriesColumns class.
 */

$plugin = array(
  'name' => 'We have to select only needed columns',
  'handler' => array(
    'class' => 'PerformanceToolkitSelectQueriesColumns',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'query_columns',
    'name' => t('Query columns', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitSelectQueriesColumns implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t("The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations), $context);
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("We have to select only needed columns, do not us '*' it can dramatically decrease performance.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'dbQueryOneColumn',
      'dbQueryAllColumns',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbQueryOneColumn($n, & $variant) {
    $variant['title'] = 'DB select for one column';

    $variant['results'] = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->range(0, $n)
      ->execute()
      ->fetchAll();
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function dbQueryAllColumns($n, & $variant) {
    $variant['title'] = 'DB select for all columns';

    $variant['results'] = db_select('performance_toolkit_node', 'n')
      ->fields('n')
      ->range(0, $n)
      ->execute()
      ->fetchAll();
  }

}
