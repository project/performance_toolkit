<?php

/**
 * @file
 * Contains PerformanceToolkitRenderCache.
 */

$plugin = array(
  'name' => 'Render with cache',
  'handler' => array(
    'class' => 'PerformanceToolkitRenderCache',
  ),
  'category' => performance_toolkit_get_available_categories(0),
  'plugin' => array(
    'path' => 'theme2',
    'name' => t('Render cache', array(), performance_toolkit_get_context()),
  ),
  'weight' => 1,
);

class PerformanceToolkitRenderCache implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 10000;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $cache = db_select('cache', 'c')
      ->fields('c', array('cid'))
      ->condition('cid', db_like('performance_toolkit:render_cache:demonstration') . '%', 'LIKE')
      ->execute()
      ->fetchField();

    if (empty($cache)) {
      $requirements['message'] = t("Please reload the page since render cache is not exist yet.", array(), $context);
    }

    return $requirements;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("It's very efficient to use render cache wherever is possible.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'renderWithoutCache',
      'renderWithCache',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function renderWithoutCache($n, & $variant) {
    $variant['title'] = 'Without cache';

    $page['highlighted']['new_stuff'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => 'my-container'),
    );

    $text = t('Heading');
    for ($i = 0; $i < $n; $i++) {
      $page['highlighted']['new_stuff'][$i] = array(
        '#markup' => '<h2 class="my-heading">' . $text . '</h2>',
      );
    }

    $variant['results'][] = drupal_render($page);
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function renderWithCache($n, & $variant) {
    $variant['title'] = 'With cache';

    $page['highlighted']['new_stuff'] = array(
      '#type' => 'container',
      '#attributes' => array('class' => 'my-container'),
      '#cache' => array(
        'keys' => array('performance_toolkit', 'render_cache', 'demonstration'),
        'bin' => 'cache',
        'expire' => time() + 100,
        'granularity' => DRUPAL_CACHE_PER_PAGE | DRUPAL_CACHE_PER_ROLE,
      ),
    );

    $text = t('Heading');
    for ($i = 0; $i < $n; $i++) {
      $page['highlighted']['new_stuff'][$i] = array(
        '#markup' => '<h2 class="my-heading">' . $text . '</h2>',
      );
    }

    $variant['results'][] = drupal_render($page);
  }

}
