<?php

/**
 * @file
 * Contains PerformanceToolkitStaticNodeLoad class.
 */

$plugin = array(
  'name' => 'Load nodes entities with static cache',
  'handler' => array(
    'class' => 'PerformanceToolkitStaticNodeLoad',
  ),
  'category' => performance_toolkit_get_available_categories(0),
  'plugin' => array(
    'path' => 'node_load_static',
    'name' => t('Node Static', array(), performance_toolkit_get_context()),
  ),
  'weight' => -100,
);

class PerformanceToolkitStaticNodeLoad implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 100;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t("The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations), $context);
    }

    return $requirements;
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t("Using static cache for repeatable calculation is very fast and efficient.", array(), $context);
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'nodeLoad',
      'nodeLoadStatic',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function nodeLoad($n, & $variant) {
    $variant['title'] = 'Node load Static';

    $existing_node = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->execute()
      ->fetchField();

    for ($i = 1; $i < $n; $i++) {
      $variant['results'][] = node_load($existing_node);
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function nodeLoadStatic($n, & $variant) {
    $variant['title'] = 'Node load without Static';

    $existing_node = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->execute()
      ->fetchField();

    for ($i = 1; $i < $n; $i++) {
      $variant['results'][] = node_load($existing_node, NULL, TRUE);
    }
  }

}
