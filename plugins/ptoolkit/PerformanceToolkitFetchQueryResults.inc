<?php
/**
 * @file
 * Contains PerformanceToolkitFetchQueryResults class`.
 */

$plugin = array(
  'name' => 'Fetch query results',
  'handler' => array(
    'class' => 'PerformanceToolkitFetchQueryResults',
  ),
  'category' => performance_toolkit_get_available_categories(1),
  'plugin' => array(
    'path' => 'fetch',
    'name' => t('Query fetch', array(), performance_toolkit_get_context()),
  ),
  'weight' => 2,
);

class PerformanceToolkitFetchQueryResults implements PerformanceToolkitInterface {

  /**
   * The plugin definition for this instance.
   */
  protected $plugin;

  /**
   * The amount of iterations for the test.
   */
  public $iterations = 500;

  /**
   * Constructor for the report plugin class.
   */
  public function __construct($plugin) {
    $this->plugin = $plugin;
  }

  /**
   * Check and returns the plugin requirements.
   */
  public function pluginRequirements($iterations, $context) {
    $requirements = array();
    $nodes = db_select('performance_toolkit_node', 'n')
      ->fields('n', array('nid'))
      ->condition('status', 0, '!=')
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($nodes < $iterations) {
      $requirements['message'] = t(
        "The amount of nodes - %nodes is less then amount of iterations - %i, so results may not be demonstrative",
        array('%nodes' => $nodes, '%i' => $iterations),
        $context
      );
    }

    return $requirements;
  }

  /**
   * We have to be prepared.
   */
  public function setUp($n) {
    db_query('RESET QUERY CACHE');
  }

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context) {
    $conclusion = t(
      "Do not use fetchAll in order to modify query results, we can use the power of iterators.",
      array(),
      $context
    );
    return $conclusion;
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function exportResults() {
    return array(
      'queryFetchAll',
      'queryFetch',
      'queryWithIterator',
    );
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function queryFetch($n, & $variant) {
    $variant['title'] = 'Query with Fetch';

    for ($k = 0; $k < $n; $k++) {

      // Build and run the query.
      $results = db_select('performance_toolkit_node', 'n')
        ->fields('n', array('nid', 'title'))
        ->condition('status', 0, '!=')
        ->range(0, $n)
        ->execute();

      $nodes = array();

      // We can use a foreach loop on the $results object.
      while ($row = $results->fetchAssoc()) {
        // Each $row is an object.
        $nodes[$row['nid']] = $row['title'];
      }
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function queryFetchAll($n, & $variant) {
    $variant['title'] = 'Query with Fetch All';

    for ($k = 0; $k < $n; $k++) {

      // Build and run the query.
      $results = db_select('performance_toolkit_node', 'n')
        ->fields('n', array('nid', 'title'))
        ->condition('status', 0, '!=')
        ->range(0, $n)
        ->execute()
        ->fetchAllAssoc('nid');

      $nodes = array();

      foreach ($results as $result) {
        $nodes[$result->nid] = $result->title;
      }
    }
  }

  /**
   * The callback function with used to export results to a page.
   */
  public function queryWithIterator($n, & $variant) {
    $variant['title'] = 'Query with iterator';

    for ($k = 0; $k < $n; $k++) {

      $results = db_select('performance_toolkit_node', 'n')
        ->fields('n', array('nid', 'title'))
        ->condition('status', 0, '!=')
        ->range(0, $n)
        ->execute();

      $nodes = array();

      // We can use a foreach loop on the $results object.
      foreach ($results as $row) {
        // Each $n is an object.
        $nodes[$row->nid] = $row->title;
      }

    }
  }

}
