<?php

/**
 * @file
 * Install file for Performance compare module.
 */

/**
 * Implements hook_schema().
 */
function performance_toolkit_schema() {
  $node  = drupal_get_schema_unprocessed('node', 'node');
  $users = drupal_get_schema_unprocessed('user', 'users');

  $schema = array(
    PERFORMANCE_TOOLKIT_NODE_TABLE => $node,
    PERFORMANCE_TOOLKIT_USERS_TABLE => $users,
  );

  return $schema;
}

/**
 * Implements hook_install().
 */
function performance_toolkit_install() {
  $nodes = 25000;
  $users = 1000;

  // Generate records in node and users clone tables.
  performance_toolkit_create_user_rows($users);
  performance_toolkit_create_node_rows($nodes);
}

/**
 * Helper to create users records in performance_toolkit_users db table.
 */
function performance_toolkit_create_user_rows($users) {
  $names = array();

  // Load password hashing API.
  require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');

  $url = parse_url($GLOBALS['base_url']);

  while (count($names) < $users) {
    $name = performance_toolkit_generate_word(mt_rand(6, 12));
    $names[$name] = $name;
  }

  $user = array(
    'uid'     => '',
    'name'    => '',
    'pass'    => user_hash_password('drupal'),
    'mail'    => '',
    'status'  => 1,
    'created' => '',
    'access'  => '',
    'login'   => '',
    'picture' => 0,
  );

  $query = db_insert(PERFORMANCE_TOOLKIT_USERS_TABLE)->fields(array_keys($user));
  foreach (array_values($names) as $uid => $name) {
    $user['uid']     = $uid;
    $user['name']    = $name;
    $user['mail']    = $name . '@' . $url['host'] . '.invalid';
    $user['created'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);
    $user['access']  = $user['created'];
    $user['login']   = $user['created'];

    $query->values(array_values($user));
  }

  $query->execute();
}

/**
 * Helper to create nodes records in performance_toolkit_nodes db table.
 */
function performance_toolkit_create_node_rows($nodes) {
  $node_types = array(
    'article' => 'article',
    'page'    => 'page',
  );

  $result = db_query_range("SELECT uid FROM {" . PERFORMANCE_TOOLKIT_USERS_TABLE . "}", 0, 50);
  foreach ($result as $record) {
    $users[$record->uid] = $record->uid;
  }

  $node = array(
    'uid'     => '',
    'vid'     => '',
    'type'    => '',
    'title'   => '',
    'promote' => '',
    'created' => '',
    'changed' => '',
    'sticky'  => '',
  );

  $query = db_insert(PERFORMANCE_TOOLKIT_NODE_TABLE)->fields(array_keys($node));
  for ($i = 0; $i < $nodes; $i++) {
    $node['uid']     = array_rand(array_values($users));
    $node['vid']     = $i + 1;
    $node['type']    = array_rand($node_types);
    $node['promote'] = mt_rand(0, 1);
    $node['sticky']  = mt_rand(0, 1);
    $node['created'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);
    $node['changed'] = REQUEST_TIME - mt_rand(0, 3600 * 24 * 30);

    if (function_exists('devel_create_greeking')) {
      $node['title'] = devel_create_greeking(1, TRUE);
    }
    else {
      $node['title'] = 'Test title for performance compare module';
    }

    $query->values(array_values($node));
  }

  $query->execute();
}

/**
 * Implements hook_requirements().
 */
function performance_toolkit_requirements($phase) {
  $requirements = array();
  $t = get_t();
  if (($phase === 'install' || $phase === 'runtime') && function_exists('libraries_get_path')) {
    $library_name = 'highcharts';
    $library_path = libraries_get_path($library_name);
    if (!$library_path) {
      $requirements[$library_name] = array(
        'title' => $t('Missing library !library', array('!library' => $library_name)),
        'severity' => REQUIREMENT_ERROR,
        'description' => $t('Performance Toolkit module requires !library library, which is missing.
         Please refer to the module %page on Drupal.org for the info.',
          array(
            '!library' => $library_name,
            '%page' => 'https://www.drupal.org/project/performance_toolkit',
          )
        ),
      );
    }
  }

  return $requirements;
}
