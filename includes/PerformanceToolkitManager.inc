<?php

/**
 * @file
 * Contains PerformanceToolkitManager class.
 */

/**
 * The class for managing reports.
 */
class PerformanceToolkitManager {

  /**
   * Basic factory function for report plugins.
   */
  public static function factory($plugin_name) {
    $ctools_plugin = FALSE;

    // Make sure that the handler class exists and that it has
    // this class as one of its parents.
    if (class_exists($plugin_name['handler']['class'])) {
      $ctools_plugin = new $plugin_name['handler']['class']($plugin_name);
    }
    else {
      // Can no find the plugin handler class.
      watchdog(
        'ptoolkit',
        'Class <em>!name</em> is not exists!',
        array('!name' => $plugin_name['handler']['class']),
        WATCHDOG_ERROR
      );
    }

    return $ctools_plugin;
  }

}
