<?php

/**
 * @file
 * Contains PerformanceToolkitInterface interface.
 */

/**
 * The interface for export format.
 */
interface PerformanceToolkitInterface {

  /**
   * The callback function which used to provide a conclusion.
   */
  public function exportConclusion($context);

  /**
   * The callback function which used to export results to a file.
   */
  public function exportResults();

}
