<?php

/**
 * @file
 * This file includes page callbacks for Performance compare module.
 */

/**
 * Performance center page callback.
 */
function performance_toolkit_page_callback($current_category) {
  $context = performance_toolkit_get_context();

  $output = array();
  $render = $source_code = array();

  drupal_set_title(t('Performance Compare center', array(), $context));

  // Get all performance compare plugins.
  $plugins = performance_toolkit_get_compare_plugins();

  if (!empty($plugins)) {
    foreach ($plugins as $plugin) {
      $path = performance_toolkit_get_plugin_path($plugin);
      if (empty($path) || $current_category != $path) {
        continue;
      }

      // Create an instance object for the plugin.
      $plugin_instance   = PerformanceToolkitManager::factory($plugin);
      $plugin_class_name = $plugin['handler']['class'];

      $callbacks  = $plugin_instance->exportResults();
      $iterations = !empty($plugin_instance->iterations) ? $plugin_instance->iterations : PERFORMANCE_TOOLKIT_ITERATIONS;

      // Check the plugin requirements.
      if (is_callable(array($plugin_instance, 'pluginRequirements'))) {
        $requirements = $plugin_instance->pluginRequirements($iterations, $context);

        if (isset($requirements['access']) && $requirements['access'] === FALSE) {
          drupal_set_message($requirements['message'], 'error');
          continue;
        }

        if (!empty($requirements['message'])) {
          drupal_set_message($requirements['message'], 'warning');
        }
      }

      foreach ($callbacks as $callback) {
        if (is_callable(array($plugin_instance, $callback))) {
          // Get source code of callbacks.
          $source_code[] = performance_toolkit_get_function_text($plugin_class_name, $callback);

          // Launch callback and collect all statistics data.
          $output[$callback] = performance_toolkit_launch_callback($plugin_instance, $callback, $iterations);

          // Show callback results for debug purposes.
          performance_toolkit_show_callback_output($output[$callback], $plugin_instance);
        }
      }

      // Add common render properties to array.
      performance_toolkit_prepare_render($render, $plugin);
      $output = performance_toolkit_render_plugin_results_charts($output, $plugin['name']);

      if (!empty($output)) {
        // Show the conclusion.
        $render[$plugin_class_name]['conclusion'] = array(
          '#prefix' => '<h2 class="performance-plugin-conclusion">',
          '#markup' => $plugin_instance->exportConclusion($context),
          '#suffix' => '</h2>',
        );

        // The plugin results comparison.
        $render[$plugin_class_name]['data'] = array(
          '#markup' => $output,
        );

        // Iterations of the plugin.
        $render[$plugin_class_name]['iterations'] = array(
          '#prefix' => '<h4 class="performance-plugin-iterations">',
          '#markup' => t('Iterations $n - %n', array('%n' => $iterations), performance_toolkit_get_context()),
          '#suffix' => '</h4>',
        );

        // Source code of plugin's callbacks.
        foreach ($source_code as $callback => $callback_code) {
          $render[$plugin_class_name]['source'][$callback] = array(
            '#prefix' => '<pre><code class="lang-php">',
            '#markup' => $callback_code,
            '#suffix' => '</code></pre>',
          );
        }
      }
    }
  }

  return $render;
}

/**
 * Launch callback and collect all performance info.
 */
function performance_toolkit_launch_callback($plugin_instance, $callback, $iterations) {
  $callback_data = performance_toolkit_get_default_variant();

  // Launch setUp method if need. Usually used to clear caches
  // and do some preparations to get more reasonable results.
  // The method will be launched for all callbacks just before
  // the callback execution.
  if (is_callable(array($plugin_instance, 'setUp'))) {
    $plugin_instance->setUp($iterations);
  }

  // Get the memory usage we have to measure memory consumption as well.
  // gc_collect_cycles();
  $memory_before = memory_get_usage();

  performance_toolkit_start_timer_for_variant($callback);
  $plugin_instance->$callback($iterations, $callback_data);
  performance_toolkit_read_timer_for_variant($callback_data, $callback);

  $memory_after = memory_get_usage();
  $callback_data['memory'] = $memory_after - $memory_before;

  return $callback_data;
}

/**
 * Helper to initialize render array.
 */
function performance_toolkit_prepare_render(& $render, $plugin) {
  $t = get_t();

  $render['#prefix'] = '<div class="performance-compare-form">';
  $render['#suffix'] = '</div>';

  $render[$plugin['handler']['class']] = array(
    '#type'  => 'fieldset',
    '#title' => $t($plugin['name'], array(), performance_toolkit_get_context()),
  );

  performance_toolkit_add_attached_files($render);
}

/**
 * Render results of code execution.
 */
function performance_toolkit_render_chart_results($output, $callback) {
  $context = performance_toolkit_get_context();

  $title = t('Expenses of Time', array(), $context);
  $chart = array(
    '#chart_id' => $callback,
    '#title'    => chart_title($title, '00cccc', 16),
    '#type'     => CHART_TYPE_PIE_3D,
    '#size'     => chart_size(PERFORMANCE_TOOLKIT_CHART_SIZE_WIDTH, PERFORMANCE_TOOLKIT_CHART_SIZE_HEIGHT),
  );

  foreach ($output as $data) {
    $time = performance_toolkit_format_time_value($data['time']);
    $chart['#data'][]    = $time['value'];
    $chart['#labels'][]  = $time['label'];
    $chart['#legends'][] = $data['title'];
  }

  $memory_chart = $chart;
  $title = t('Memory Consumption', array(), $context);
  $memory_chart['#legends']  = array();
  $memory_chart['#title']    = chart_title($title, '00cccc', 16);
  $memory_chart['#type']     = CHART_TYPE_PIE_3D;
  $memory_chart['#chart_id'] = $callback . ' memory';
  $memory_chart['#data'] = $memory_chart['#labels'] = array();
  $memory_chart['#adjust_resolution'] = TRUE;

  foreach ($output as $data) {
    $memory = performance_toolkit_format_memory_value($data['memory']);
    $memory_chart['#data'][]   = $memory['value'];
    $memory_chart['#labels'][] = $memory['label'];
  }

  $output  = theme('chart', array('chart' => $chart));
  $output .= theme('chart', array('chart' => $memory_chart));

  return $output;
}

/**
 * Render results of code execution.
 */
function performance_toolkit_render_plugin_results_charts($output, $callback) {

  $default_chart = array(
    '#type' => 'chart',
    '#title' => t('Pie simple'),
    '#chart_type' => 'pie',
    '#chart_library' => 'highcharts',
    '#legend_position' => 'right',
    '#data_labels' => TRUE,
    '#tooltips' => TRUE,
    '#title_color' => '00cccc',
    '#title_font_size' => 16,
  );

  $time_chart['pie_data']['#type']   = 'chart_data';
  $memory_chart['pie_data']['#type'] = 'chart_data';

  $time_chart['#title']   = t('Expenses of Time');
  $memory_chart['#title'] = t('Memory consumption');

  foreach ($output as $data) {
    $time   = performance_toolkit_format_time_value($data['time']);
    $memory = performance_toolkit_format_memory_value($data['memory']);

    $time_chart['pie_data']['#data'][]    = $time['value'];
    $time_chart['pie_data']['#labels'][]  = $data['title'] . ' (' . $time['label'] . ')';
    $time_chart['pie_data']['#legends'][] = $data['title'];

    $memory_chart['pie_data']['#data'][]   = $memory['value'];
    $memory_chart['pie_data']['#labels'][] = $memory['label'];
  }

  $example['time']   = array_merge($default_chart, $time_chart);
  $example['memory'] = array_merge($default_chart, $memory_chart);
  return drupal_render($example);
}

/**
 * Render results of code execution.
 */
function performance_toolkit_show_callback_output($callback_data, $plugin_instance) {
  if (!function_exists('dpm')) {
    return;
  }

  $debug = FALSE;
  if (PERFORMANCE_TOOLKIT_SHOW_EVALUATION_RESULTS || !empty($plugin_instance->debug)) {
    $debug = TRUE;
  }

  if (!empty($callback_data['results']) && $debug) {
    if (function_exists('dpm')) {
      dpm($callback_data['results']);
    }
    else {
      print_r($callback_data['results']);
    }
  }
}

/**
 * Get the source code of the callback.
 */
function performance_toolkit_get_function_text($class, $method) {
  $func = new ReflectionMethod($class, $method);

  $filename = $func->getFileName();
  $start_line = $func->getStartLine() - 1;
  $end_line = $func->getEndLine();
  $length = $end_line - $start_line;

  $source = file($filename);
  $code = implode("", array_slice($source, $start_line, $length));

  return $code;
}

/**
 * Get the source code of the callback.
 */
function performance_toolkit_add_attached_files(& $render) {
  $module_path = drupal_get_path('module', 'performance_toolkit');
  $render['#attached']['css'][] = $module_path . '/css/perf_plugin.css';
}
